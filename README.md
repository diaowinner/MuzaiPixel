<div align=right>下载，请点击“发行版”下面的蓝链 →</div>

![目哉像素](chusung_03(1).png)
# 目哉像素
目哉像素（曾用名 k8x12j）是一个基于 k8x12 的 8x12 中日文点阵（像素）字体。  
这是 Gitee 镜像，仅为下载时为国人提供便利。在 [GitHub](https://github.com/DWNfonts/MuzaiPixel) 或 [itch.io](https://diaowinner.itch.io/muzaipixel) 上查看该字体。
## 字体内容
* MuzaiPixel (original).ttf is tuned for square pixels.  
original 版本像素是方的，如果你的工具没有 bug 的话建议用这个
* MuzaiPixel (flat).ttf has full-width chars. If there's something wrong with your tool, use that, and use text effect to make it 8x12.  
flat 版本拉宽到原来的 1.5 倍。如果你的工具有 bug，那就用这个，然后压缩一下。

These fonts cannot install both. They have the same font metadatas.
这些字体不能同时安装，他们有共同的元数据。
## 协议
版权所有 Copyright © 2015-2021 Num Kadoma, 2022-2023 diaowinner.

These fonts are free software. Unlimited permission is granted to use, copy, and distribute them, with or without modification, either commercially or noncommercially. THESE FONTS ARE PROVIDED "AS IS" WITHOUT WARRANTY.  
这些字体是自由软件。 您可以自由使用、复制和重新分发，无论是否进行任何修改或用于商业用途。这些字体 “按原样” 提供，不做担保。